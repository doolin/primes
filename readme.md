# Primes

Primes is a simple gem for computing and displaying an outer product
matrix of the first 10 prime numbers in tabular output to a tty, or any
other device accepting a string.

## Quick start

Ruby 2.3.1 is specified for rbenv and rvm users.
rvm users have been provided with .ruby-gemset file for convenience.

* git clone https://doolin@bitbucket.org/doolin/primes.git
* cd primes
* bundle install
* ./bin/commandline

For more fun:

* ./bin/console
    * `Primes::Table.first_10`
    * `Primes::Table.new(4, 7).build.print`

## Notes

* The general idea for finding the first 10 primes is to
  estimate the value of a slightly larger prime, compute
  Sieve of Eratosthenes for a sequence up the estimate,
  then extract the first 10 primes using the sieve.

* Many commits are paired spec and implementation,
  some series of commits follow red/green/refactor.

* Seattle-style arguments, just for fun.

### Memory requirements

As of this writing, the `Primes::Generator` implementation isn't
particulary memory efficient for two reasons. First, the sieve is stored
as an array of Ruby booleans (e.g., `[false, false, true, true, ...]`).
This would be much more efficiently stored as a bitset. Second, each
series is generated from scratch each time. It would be much more
efficient to implement a lazy sequence, where each indicator (`true`,
`false` or `0`, `1`) was computed only once. Values past the end of the
current sequence would be computed on-the-fly.

Due to how the Sieve of Eratosthenes works, it's not clear (to me) if
this is viable. However, it seems like an interesting problem, so there is
bound to be a way to do it.

`Primes::Table` creates the entire array of the outer product, and holds it
in memory. One way to reduce memory footprint is to compute each row at
run time, as needed. This could probably also be done in a lazy manner.

### Runtime performance

Cursory reading indicates Sieve of Eratosthenes is pretty good with
respect to performance, certainly with small sequences. For a single
use, with small sequences, the implementation seems adequate.

The previous remarks with respect to memory requirements also apply to
runtime performance. Previous results are not saved, hence repeated
invocations will recompute the sieve from the beginning. Same
observation applies to the table for printing: resizing the table
(a different number of primes) triggers recomputation.

### Benchmarking

Very crude benchmarking to get some ballpark notions of how expensive
it is to generate primes.

#### Runtime and memory

Fast, "back-of-the-envelope" performance data.

What to expect:

    * Sieve runs in O(nlog log n)

    * Trial division is not quite O(n^2) as the inner loop to
      check primality runs to sqrt(n) instead of n.

These are all single runs with Time/Memory for GC enabled. Disabling GC
doesn't show any obvious difference in the Sieve data, GC was not
disabled for the trial division.

Result format is Time(s)/Memory(M).

```
     count |     Sieve        | Trial division
---------------------------------------------
           | no GC |   GC     | GC
---------------------------------------------
      1000 | 0.002 | 0.002/0  | 0.002/0
    10_000 | 0.03  | 0.03/1   | 0.06/0
   100_000 | 0.4   | 0.4/10   | 1.5/1
 1_000_000 | 5     | 5/113    | 47/8
10_000_000 | 63    | 68/85    | 1569/-2 (!?)
20_000_000 | 154   | 136/185  | ----
30_000_000 | 230   | 213/280  | ----
```

The upshot is a somewhat predictable result. The sieve takes
up a lot more memory as it's keeping an array of `true/false` for
every number up to the count as well as an array of the primes.
It does however run much faster than trial division at higher counts.
However, the trial division only keeps the primes hence using less
memory.

The sieve would use less memory with a bitset to indicate which
numbers are prime.

The -2 result for memory usage on the 10_000_000 count trial
division experiment will be investigated later.

The Sieve of Eratosthenes seems like a good balance between runtime and
memory usage (using a bitset), with the added benefit of being
relatively easy to program.

## TODO

* Implement lazy.

* Implement bitset instead of array of `true/false`. Since Ruby doesn't
  have a native bitset, this will require installing a bitset gem.


## Client's Objective

Write a program that prints out a multiplication table of the first 10
prime number.

* The program must run from the command line and print one table to
STDOUT.

* The first row and column of the table should have the 10 primes, with
each cell containing the product of the primes for the corresponding row and
column.

### Notes from client

* Consider complexity. How fast does your code run? How does it scale?

* Consider cases where we want N primes.

* Do not use the Prime class from stdlib (write your own code).

* Write tests. Try to demonstrate TDD/BDD.

* If you’re using external dependencies please specify those
dependencies and how to install them.

* Please package your code, OR include running instructions.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'primes'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install primes

## Contributing

Bug reports and pull requests are welcome on GitHub at
https://github.com/[USERNAME]/primes. This project is intended to be a
safe, welcoming space for collaboration, and contributors are expected
to adhere to the [Contributor Covenant](http://contributor-covenant.org)
code of conduct.


## License

The gem is available as open source under the terms of the [MIT
License](http://opensource.org/licenses/MIT).

