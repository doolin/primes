lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'primes/version'

Gem::Specification.new do |spec|
  spec.name          = "primes"
  spec.version       = Primes::VERSION
  spec.authors       = ["Dave Doolin"]
  spec.email         = ["david.doolin@gmail.com"]

  spec.summary       = %q{Table of first 10 primes}
  spec.homepage      = "http://github.com/doolin/primes"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.metadata['rubygems_mfa_required'] = 'true'
end
