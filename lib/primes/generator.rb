# frozen_string_literal: true

require 'singleton'
require 'pry'
require 'ap'

# Primes module for various interesting exercises
# with prime numbers.
module Primes
  # Generate prime numbers in sequence
  class Generator
    include Singleton

    def self.first n
      primes = extractor sieve estimate n
      primes[0...n]
    end

    # rubocop:disable Metrics/MethodLength
    def self.sieve n
      sieve = Array.new n, true
      sieve[0] = sieve[1] = false
      (2..Math.sqrt(n)).to_a.each do |number|
        k = 0
        next unless sieve[number]

        loop do
          j = (number * number) + (k * number)
          break if j > n

          sieve[j] = false
          k += 1
        end
      end
      sieve
    end
    # rubocop:enable Metrics/MethodLength

    def self.extractor sieved
      i = 0
      (0...sieved.size).select do
        i += 1
        sieved[i - 1]
      end
    end

    # prime number theorem: π(n) ～ n/ln n,
    # where the accuracy improves asymptotically.
    def self.estimate n
      n += 3 # overestimate, room to spare
      (n * Math.log(n)).to_i
    end

    def self.primes_less_than number
      extractor sieve number
    end

    def self.prime? n
      return false if n <= 1
      return true if n <= 3
      return false if n.even? || (n % 3).zero?

      i = 5
      while i * i <= n
        return false if (n % i).zero? || (n % (i + 2)).zero?

        i += 6
      end
      true
    end

    def self.trial_division n
      primes = []
      # bigger hammer than we really need
      (2..1_000_000_000).each do |number|
        break if primes.length == n

        primes << number if Generator.prime? number
      end
      primes
    end
  end
end
