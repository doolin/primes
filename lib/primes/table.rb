# frozen_string_literal: true

require 'singleton'
require 'pry'
require 'ap'
require_relative 'generator'

# Primes module for various interesting exercises
# with prime numbers.
module Primes
  # Print the primes in a useful format
  class Table
    attr_reader :table

    CELL_FORMAT = '%8d |'

    def initialize n, m = nil
      @n = n
      @m = m || n
    end

    def primes
      @table
    end

    def self.first_10
      puts first_n 10
    end

    def self.first_n n
      Table.new(n).build.format_full_table
    end

    def build
      @row = Generator.first @n
      @column = Generator.first @m

      @table = []
      @row.each do |r|
        @table << @column.map { |c| r * c }
      end
      self
    end

    def print
      puts format_full_table
    end

    def format
      (CELL_FORMAT * @m).to_s
    end

    def format_row row
      sprintf(format % row)
    end

    def format_table
      formatted_table = []
      @table.each do |row|
        formatted_table << format_row(row)
      end
      formatted_table.join("\n")
    end

    def format_full_table
      formatted_table = ["\t |#{format_row(@column)}"]
      @table.each_with_index do |row, i|
        formatted_table << ((CELL_FORMAT % @column[i]) + format_row(row))
      end
      formatted_table.join("\n")
    end
  end
end
