# frozen_string_literal: true

require_relative '../spec_helper'
require_relative '../../lib/primes/generator'

module Primes
  describe Generator do
    before :all do
      @expected = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    end

    describe 'first_10' do
      it 'finds the first 10 with class method' do
        expect(described_class.first(10)).to eq @expected
      end
    end

    describe 'estimate' do
      it 'estimates lower bound for kth prime' do
        expect(described_class.estimate(2)).to eq 8
        expect(described_class.estimate(3)).to eq 10
        expect(described_class.estimate(5)).to eq 16
        expect(described_class.estimate(10)).to eq 33
      end
    end

    describe 'sieve' do
      before :all do
        @sieved = [
          false, false, true, true, false, true, false, true,
          false, false, false, true, false, true, false, false
        ]
        @count = 15
      end

      it 'sieves correctly' do
        expect(described_class.sieve(@count)).to eq @sieved
      end

      it 'extracts primes correctly' do
        expected = [2, 3, 5, 7, 11, 13]
        expect(described_class.extractor(@sieved)).to eq expected
      end
    end

    describe '.primes_less_than n' do
      it 'creates a list of prime numbers less than n' do
        expect(described_class.primes_less_than(30)).to eq @expected
      end
    end

    describe 'trial_division' do
      it 'finds sequence by successive trial division' do
        expect(described_class.trial_division(10)).to eq @expected
      end
    end

    describe 'prime?' do
      it 'finds a correct prime' do
        expect(described_class.prime?(10)).to be false
        expect(described_class.prime?(7)).to be true
        expect(described_class.prime?(29)).to be true
        expect(described_class.prime?(100)).to be false
      end
    end
  end
end
