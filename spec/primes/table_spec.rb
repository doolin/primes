# frozen_string_literal: true

require_relative '../spec_helper'
require_relative '../../lib/primes'

module Primes
  describe Table do
    describe '.table' do
      it 'generates a table' do
        expected = [
          [4, 6, 10],
          [6, 9, 15]
        ]
        table = described_class.new(2, 3).build
        expect(table.primes).to eq expected
      end
    end

    describe 'table of first n primes' do
      it 'prints outer product table of first n primes' do
        # rubocop:disable Layout/LineLength
        expected = "\t |       2 |       3 |\n       2 |       4 |       6 |\n       3 |       6 |       9 |"
        # rubocop:enable Layout/LineLength
        expect(described_class.first_n(2)).to eq expected
      end
    end

    describe 'formatting' do
      before do
        @table = described_class.new(2, 3).build
      end

      describe 'printf formatting' do
        it 'sets the cell format' do
          expect(Table::CELL_FORMAT).to eq '%8d |'
        end

        it 'creates a formatting string' do
          expected = '%8d |%8d |%8d |'
          expect(@table.format).to eq expected
        end

        it 'prints a formatted row' do
          expected = '       4 |       6 |      10 |'
          expect(@table.format_row(@table.table.first)).to eq expected
        end

        it 'prints a formatted table' do
          expected = "       4 |       6 |      10 |\n       6 |       9 |      15 |"
          expect(@table.format_table).to eq expected
        end

        it 'prints a formatted table with top row padding' do
          # rubocop:disable Layout/LineLength
          expected = "\t |       2 |       3 |       5 |\n       2 |       4 |       6 |      10 |\n       3 |       6 |       9 |      15 |"
          # rubocop:enable Layout/LineLength
          expect(@table.format_full_table).to eq expected
        end
      end
    end
  end
end
